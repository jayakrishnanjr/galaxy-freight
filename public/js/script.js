/*aos js script starts*/

AOS.init({
    duration: 1200,
  })


/*aos script ends*/
/*gallery script starts*/

$('[data-fancybox="img"]').fancybox({
  buttons : [ 
  'slideShow',
  'share',
  'zoom',
  'fullScreen',
  'close',
  'thumbs'
],
thumbs : {
 // autoStart : true,
}
});
// sorting
$(document).ready(function(){
  $("#all").click(function(){
    $(".event-img,.company-img").show();
  });
  $("#events").click(function(){
    $(".event-img").show(),
    $(".company-img").hide();
  });
   $("#company").click(function(){
    $(".company-img").show(),
    $(".event-img").hide();
  });
});


/*gallery script ends*/